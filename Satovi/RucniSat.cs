﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satovi
{
    public class RucniSat : Sat
    {
        private Tip tip;
        private float tezina;
        private bool pozlata;

        public RucniSat() { }

        public RucniSat(string a, string b, Pol c, int d, float e, Tip f, float g, bool h)
            : base(a, b, c, d, e)
        {
            this.tip = f;
            this.tezina = g;
            this.pozlata = h;
        }

        public override float RacunajPopularnostSata => base.RacunajPopularnostSata * 1.5f * (this.pozlata ? 1.35f : 1);

        public override void Prikazi()
        {
            base.Prikazi();
            Console.WriteLine($"Tip = {this.tip}");
            Console.WriteLine($"Tezina = {this.tezina} grama");
            Console.WriteLine($"Pozlata = {(this.pozlata ? "Da" : "Ne")}");
        }

        public override float Cena => base.Cena;


    }
}
