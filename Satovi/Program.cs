﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satovi
{
    class Program
    {
        static void Main(string[] args)
        {
            Sat[] satovi = new Sat[30];

            Sat.tretnutnaGodina = 2019;

            Console.WriteLine("Unesi broj satova koji su na prodaji");

            int brojSatova = int.Parse(Console.ReadLine());

            for (int i = 0; i < brojSatova; i++)
            {
                Console.WriteLine("Unesite proizvodjaca sata:");
                string a = Console.ReadLine();
                Console.WriteLine("Unesi model sata:");
                string b = Console.ReadLine();
                Console.WriteLine("Unesi pol sata: (za muskarce, za zene, univerzalan)");
                string pol = Console.ReadLine().ToLower();
                Pol c = Pol.ZaMuskarce;
                switch (pol)
                {
                    case "za muskarce":
                        c = Pol.ZaMuskarce;
                        break;
                    case "za zene":
                        c = Pol.ZaZene;
                        break;
                    case "univerzalan":
                        c = Pol.Univerzalan;
                        break;
                    default:
                        Console.WriteLine("Uneli ste nepostojeci pol!");
                        break;
                }
                Console.WriteLine("Unesi godinu proizvodnje:");
                int d = int.Parse(Console.ReadLine());
                Console.WriteLine("Unesi cenu u evrima:");
                float e = float.Parse(Console.ReadLine());
                Console.WriteLine("Unesi vrstu sata: rucni / zidni");
                string vrstaSata = Console.ReadLine().ToLower();
                if (vrstaSata == "rucni")
                {
                    Console.WriteLine("Unesi tip sata: (automatik, kvarcni, solarni)");
                    string tip = Console.ReadLine().ToLower();
                    Tip f = Tip.Automatik;
                    switch (tip)
                    {
                        case "automatik":
                            f = Tip.Automatik;
                            break;
                        case "kvarcni":
                            f = Tip.Kvarcni;
                            break;
                        case "solarni":
                            f = Tip.Solarni;
                            break;
                        default:
                            Console.WriteLine("Uneli ste nepostojeci tip!");
                            break;
                    }
                    Console.WriteLine("Unesite tezinu sata u gramima:");
                    float g = float.Parse(Console.ReadLine());
                    Console.WriteLine("Unesite pozlatu: da/ne");
                    string pozlata = Console.ReadLine().ToLower();
                    bool h = pozlata == "da" ? true : false;
                    satovi[i] = new RucniSat(a, b, c, d, e, f, g, h);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Unesite duzinu sata u cm");
                    int f = int.Parse(Console.ReadLine());
                    Console.WriteLine("Unesite sirinu sata u cm");
                    int g = int.Parse(Console.ReadLine());
                    Console.WriteLine("Unesite visinu sata u cm ");
                    int h = int.Parse(Console.ReadLine());
                    satovi[i] = new ZidniSat(a, b, c, d, e, f, g, h);
                    Console.WriteLine();
                }
            }

            for (int i = 0; i < brojSatova; i++)
            {
                Console.WriteLine($"Sat broj {i + 1}:");
                satovi[i].Prikazi();
                Console.WriteLine();
            }

            Console.WriteLine("Popularnost satova:");
            for (int i = 0; i < brojSatova; i++)
                Console.WriteLine($"Sat broj {i + 1} = {satovi[i].RacunajPopularnostSata}");
            Console.WriteLine();

            int maxPop = 0;
            for (int i = 0; i < brojSatova; i++)
                for (int j = 0; j < brojSatova; j++)
                    if (satovi[i].RacunajPopularnostSata > satovi[maxPop].RacunajPopularnostSata)
                        maxPop = i;

            Console.WriteLine($"Najpopularniji sat trenutno na prodaji je:");
            Console.WriteLine();
            satovi[maxPop].Prikazi();
            Console.WriteLine();

            int maxCena = 0;
            for (int i = 0; i < brojSatova; i++)
                for (int j = 0; j < brojSatova; j++)
                    if (satovi[i].Cena > satovi[maxCena].Cena)
                        maxCena = i;

            Console.WriteLine($"Najskuplji sat trenutno na prodaji je:");
            Console.WriteLine();
            satovi[maxCena].Prikazi();
            Console.WriteLine();

        }
    }
}
