﻿using System;

namespace Satovi
{
    public class ZidniSat : Sat
    {
        private int duzina;
        private int sirina;
        private int visina;

        public ZidniSat() { }

        public ZidniSat(string a, string b, Pol c, int d, float e, int f, int g, int h)
            : base(a, b, c, d, e)
        {
            this.duzina = f;
            this.sirina = g;
            this.visina = h;
        }

        public override float RacunajPopularnostSata => base.RacunajPopularnostSata + (this.duzina + this.sirina + this.visina) / 100;

        public override void Prikazi()
        {
            base.Prikazi();
            Console.WriteLine($"Duzina = {this.duzina}cm");
            Console.WriteLine($"Sirina = {this.sirina}cm");
            Console.WriteLine($"Visina = {this.visina}cm");
        }

        public override float Cena => base.Cena;
    }
}
