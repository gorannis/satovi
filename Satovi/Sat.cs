﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Satovi
{
    public class Sat
    {
        protected string proizvodjac;
        protected string model;
        protected Pol pol;
        protected int godinaProizvodnje;
        protected float cena;
        public static int tretnutnaGodina;
        
        public Sat() { }

        public Sat(string a, string b, Pol c, int d, float e)
        {
            this.proizvodjac = a;
            this.model = b;
            this.pol = c;
            this.godinaProizvodnje = d;
            this.cena = e;
        }

        public virtual float RacunajPopularnostSata => 1 / ((tretnutnaGodina - (this.godinaProizvodnje = this.godinaProizvodnje == 2019 ? 2018 : this.godinaProizvodnje)) * this.cena);

        public virtual float Cena
        {
            get
            {
                return this.cena;
            }
        }

        public virtual void Prikazi()
        {
            Console.WriteLine($"Proizvodjac = {this.proizvodjac}");
            Console.WriteLine($"Model = {this.model}");
            Console.WriteLine($"Pol = {this.pol}");
            Console.WriteLine($"Godina proizvodnje = {this.godinaProizvodnje}");
            Console.WriteLine($"Cena = {this.cena} evra");
            Console.WriteLine($"Popularnost = {this.RacunajPopularnostSata}");
        }


    }
}
